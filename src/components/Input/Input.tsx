import React from 'react';
import './Input.css';

export interface InputProps {
    className: string;
    type: string;
    placeHolder?: string;
    defaultValue?: string;
  }

const Input: React.SFC<InputProps> = ({className,defaultValue,placeHolder,type}) => {
    return (
        <input 
            className ={className}
            type={type}
            placeholder={placeHolder}
            defaultValue = {defaultValue}
        />
    );
};

export default Input;