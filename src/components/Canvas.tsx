import React from 'react';
import createEngine, { 
    DefaultLinkModel, 
    DefaultNodeModel,
    DiagramModel ,
    PortModelAlignment
  } from '@projectstorm/react-diagrams';
  
  import {
    CanvasWidget
  } from '@projectstorm/react-canvas-core';

import InnerForm from './chatNode/chatNode'  


// import the custom models
import { DiamondNodeModel } from './CustomNode/CustomNodeModel';
import { DiamondNodeFactory } from './CustomNode/CustomNodeFactory';
import { SimplePortFactory } from './CustomNode/CustomPortFactory';
import { DiamondPortModel } from './CustomNode/CustomPortModel';


const Canvas = () => {
    // create an instance of the engine with all the defaults
  const engine = createEngine();

  engine.getPortFactories().registerFactory(new SimplePortFactory('diamond', config => new DiamondPortModel(PortModelAlignment.TOP)));

  engine.getNodeFactories().registerFactory(new DiamondNodeFactory());

  const SerializeCanvas = () => {
    console.log(model.serialize());
  }
  
  // node 1
  const node1 = new DefaultNodeModel({
    name: 'Node 1',
    color: 'rgb(0,192,255)',
  });
  node1.setPosition(100, 100);
  let port1 = node1.addOutPort('Out');

  // node 2
  const node2 = new DefaultNodeModel({
    name: 'Node 1',
    color: 'rgb(0,192,255)',

  });
  node2.setPosition(150, 90);
  let port2 = node2.addOutPort('Out');
  const listener = 
  node2.registerListener({positionChanged: () => console.log("position changed")})

  const node3 = new DiamondNodeModel();
  node3.setPosition(180,180);
  node3.setInnerComponent(
  <InnerForm 
    className="UserNode"
    text="hello world"       
        
  />);
  // console.log(node3.component);  
  
  

  // link them and add a label to the link
  const link = port1.link<DefaultLinkModel>(port2);
  link.addLabel('Hello World!'); 

  
  

  const model = new DiagramModel();  
  let models = model.addAll(node1, node2,node3, link);    

  models.forEach(item => {
		item.registerListener({
      entityRemoved: (e: any) => {console.log("Entity Removed"); SerializeCanvas(); },
      selectionChanged: (e: any) => {console.log(e)}
      
		});
	});

	// model.registerListener({
	// 	eventDidFire: action('model eventDidFire')
	// });

  engine.setModel(model);
  
    return (
        <CanvasWidget engine={engine}/>
    );
};

export default Canvas;