import { DiamondNodeWidget } from './CustomNodeWidget';
import { DiamondNodeModel } from './CustomNodeModel';
import * as React from 'react';
import { AbstractReactFactory } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';

export class DiamondNodeFactory extends AbstractReactFactory<DiamondNodeModel, DiagramEngine> {
	constructor() {
		super('diamond');
	}

	generateReactWidget({model}: any): JSX.Element {
		return <DiamondNodeWidget engine={this.engine} size={100} node={model} />;
	}

	generateModel() {
		return new DiamondNodeModel();
	}
}
