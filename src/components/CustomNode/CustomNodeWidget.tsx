import React, {useState,useEffect} from "react";
import { DiamondNodeModel } from "./CustomNodeModel";
import {
  DiagramEngine,
  PortModelAlignment,
  PortWidget,
  PortModel
} from "@projectstorm/react-diagrams";
import styled from "@emotion/styled";

export interface DiamondNodeWidgetProps {
  node: DiamondNodeModel;
  engine: DiagramEngine;
  size?: number;
}

const SPort = styled.div`
  width: 10px;
  height: 10px;
  z-index: 10;
  background: white;      
  border-radius: 10px;
  &:hover {
    background: #4dffff;
  }
`;

export class DiamondNodeWidget extends React.Component<DiamondNodeWidgetProps> {
  render() {
    const size = this.props.size || 0;
    const portTop = this.props.node.getPort(
      PortModelAlignment.TOP
    ) as PortModel;
    const portBottom = this.props.node.getPort(
      PortModelAlignment.BOTTOM
    ) as PortModel;
    const innerComponent = this.props.node.getInnerComponent() as JSX.Element;

    return (
      <div className="container-node">
        <PortWidget
          className="dot-node top-node"
          port={portTop}
          engine={this.props.engine}
        >
          <SPort />
        </PortWidget>
        {innerComponent}
        <PortWidget
          className="dot-node bottom-node"
          port={portBottom}
          engine={this.props.engine}
        >
          <SPort />
        </PortWidget>
      </div>
    );
  }
}
