import React from 'react';
import Canvas from './components/Canvas';

import './App.css';


function App() {

  let SavedCanvas : string|null = localStorage.getItem('botBuilderCanvas');
  if (!SavedCanvas) {
    
  }

  

  return (
      <div className="CanvasContainer">
        <Canvas/>
      </div>
    
  );
}

export default App;
